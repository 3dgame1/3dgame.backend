const express = require('express');
const http = require('http');
const mongoose = require('mongoose');
const {Schema} = mongoose;

const mongoConnection = 'mongodb+srv://test:testtree@treejs.o6dix.mongodb.net/treejs?retryWrites=true&w=majority';

const app = express();

app.use(express.json({ type: '*/*' }));

const server = http.createServer(app);


const historySchema = new Schema({
    userId: String,
    playerPos: { type: {
        x: Number,
        y: Number,
        z: Number
    } , required: true},
    objectPos: { type: "array", items: [{
        x: Number,
        y: Number,
        z: Number
    }] },
});

const History = mongoose.model('History', historySchema);

app.use((req, res, next) => {
    res.header('Access-Control-Allow-Origin', '*');
    res.header('Access-Control-Allow-Methods', 'GET,PUT,POST,DELETE');
    res.header('Access-Control-Allow-Headers', 'Content-Type');

    next();
});


app.post('/', async (req, res) => {
    const data = req.body;

    await mongoose.connect(mongoConnection);

    const history = new History(
        {
            userId: data.userId,
            playerPos: data.playerPos,
            objectPos: data.objectPos,
        });

    History.create(history, (err, history) => { });

    res.setHeader('Content-Type', 'application/json');
    res.end(JSON.stringify(history));
});

app.put('/:id', async (req, res) => {
    const uuid = req.params.id;
    const data = req.body;

    await mongoose.connect(mongoConnection);

    History.findOne({ userId: uuid }, (err, history) => {
        history.playerPos = data.playerPos;
        history.objectPos = data.objectPos;

        history.save((err, history) => {
            res.setHeader('Content-Type', 'application/json');
            res.end(JSON.stringify(history));
        });
    });
});

app.get('/:id', async (req, res) => {
    const uuid = req.params.id;

    await mongoose.connect(mongoConnection);

    History.findOne({ userId: uuid }, (err, history) => {
        
		res.setHeader('Content-Type', 'application/json');
		res.end(JSON.stringify(history));
        
    });
});



server.listen(3000, () => {
    console.log('listening on *:3000');
});



